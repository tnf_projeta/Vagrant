# README 

### Description
Guide de lancement Vagrant

### Prérequis minimum
- Vagrant 2.3
- WSL Debian 11 (bullseye)
- (WSL) Ansible 2.10.8
- (WSL) Python Python 3.9.2

### Instructions
- Ouvrir un terminal Powershell.
- Faire un "Set-Location" vers le répertoire "tnf_projeta/'1. Vagrant'". (à la racine du répertoire contenant le vagrantfile)
- Faire un "vagrant up".
- Patienter le temps de créer les machines.
- Ouvrir une fenêtre WSL
- Cloner le répertoire ansible sur gitlab.
- Lancer le fichier ssh.
- Cloner les clés ssh.
- Tester avec ansible front -m ping.
- Connexion OK = ansible fonctionnel.
- Lancer le playbook dans le playbook/.yml.
- Attendre la fin du traitement du playbook.
- Une fois que tout est OK aller sur le navigateur en tapant l'adresse IP 192.168.0.10.
- Affichage de la page web.
